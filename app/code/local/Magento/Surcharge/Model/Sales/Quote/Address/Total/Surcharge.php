<?php
/**
 * Get modified totals in cart & checkout based on Surcharge Fee
 * @author Shivani
 *
 **/

class Magento_Surcharge_Model_Sales_Quote_Address_Total_Surcharge extends Mage_Sales_Model_Quote_Address_Total_Abstract {
    
	public function __construct() {
        $this->setCode('magento_surcharge');
    }
	
	/**
     * Get label
     *
     * @return string
     */
    public function getCode() {
        return Mage::helper('magento_surcharge')->__('surchargeamount');
    }
 
    /**
     * Get label
     *
     * @return string
     */
    public function getLabel() {
        return Mage::helper('magento_surcharge')->__('Surcharge Additional Fee');
    }
 
    /**
     * Collect totals information
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     */
    public function collect(Mage_Sales_Model_Quote_Address $address) {
        parent::collect($address);
		
		if (($address->getAddressType() == 'billing')) {
            return $this;
        }
		
		//calculate surcharge amount
		$amount = $this->_getSurchargeAmount($address);

        if ( $amount !== false ) {
            $this->_addAmount($amount);
            $this->_addBaseAmount($amount);
			//add surcharge amount to address
			$address->setBaseSurchargeAmount($amount);
        } else {
			$address->setBaseSurchargeAmount('');
		}
 
        return $this;
    }
 
    /**
     * Add totals information to address object
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        if (($address->getAddressType() == 'billing')) {
			return $this;
		}
		
		//get surcharge amount to address
		$amount = $address->getBaseSurchargeAmount();
		
		if ( $amount != 0 && $amount != '' ) {
			$address->addTotal(array(
				'code'  => $this->getCode(),
				'title' => $this->getLabel(),
				'value' => $amount
			)); //to add after discounted price
		}
 
        return $this;
    }
	
	/**
	 * get calculated surcharge amount only if specific rule is applied
	 *
	 * @return {bool,float}
	 */	
	protected function _getSurchargeAmount($address) {
		$appliedRuleIds = array();
		$amount = $flag = false;
		$_cartRules = explode(';', Mage::getStoreConfig('checkout/rules/ids'));
		$addressItems = $this->_getAddressItems($address);
		
		if( count($addressItems) > 0 ) {
			foreach ($addressItems as $item) {
				if ( !!$item && $item->getQty() > 0 ) {
					//collect all applicable rule ids at item level
					$appliedRuleIds[] = $item->getAppliedRuleIds();
				}
			}
		}
		
		//if required
		//$salesRuleIds = $this->_getSalesRuleIds();
		
		foreach($_cartRules as $ruleId) {
			//just one rule is applied then surcharge will be applicable
			if( in_array($ruleId, $appliedRuleIds) ) {
				$flag = true;
				break;
			}
		}
		
		if( $flag === true ) {
			$amount = Mage::helper('magento_surcharge')->calculateSurchargeAmount($address->getSubtotal());
		}
		
		return $amount;
	}
	
	/**
	 * get all sales rule ids
	 *
	 * @return array
	 */
	protected function _getSalesRuleIds() {
		$rules = Mage::getResourceModel('salesrule/rule_collection');
		$ruleIds = array();
		if( count($rules) > 0 ) {
			foreach($rules as $rule) {
				if( (int)$rule->getIsActive() == 1 ) {
					$ruleIds[] = (int)$rule->getId();
				}
			}
		}
		
		return $ruleIds;
	}
}
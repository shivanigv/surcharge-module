<?php
/**
 * add surcharge in Creditmemo
 * @author Shivani
 * 
 * Important Note : Creditmemo can be partial, 
 * in such a case if cart total fee is linked with items price complete fee needs to be recalculated based on invoiced/refunded items.
 **/
 
class Magento_Surcharge_Model_Total_Surcharge_Creditmemo extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract {
	
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        $order = $creditmemo->getOrder();
		
        $amount = Mage::helper('magento_surcharge')->calculateSurchargeAmount($creditmemo->getSubtotal());
		
        if ($amount) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $amount);
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $amount);
        }
 
        return $this;
    }
}
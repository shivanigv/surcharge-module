<?php
/**
 * add surcharge in invoice
 * @author Shivani
 * 
 * Important Note : Invoice can be partial, 
 * in such a case if cart total fee is linked with items price complete fee needs to be recalculated based on invoiced/refunded items.
 **/
 
class Magento_Surcharge_Model_Total_Surcharge_Invoice extends Mage_Sales_Model_Order_Invoice_Total_Abstract {
    
	public function collect(Mage_Sales_Model_Order_Invoice $invoice) {
        $order = $invoice->getOrder();
		
        $amount = Mage::helper('magento_surcharge')->calculateSurchargeAmount($invoice->getSubtotal());
		
        if ($amount) {
            $invoice->setGrandTotal($invoice->getGrandTotal() + $amount);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $amount);
        }
 
        return $this;
    }
}
<?php
class Magento_Surcharge_Block_Rewrite_Sales_Order_Invoice_Totals extends Mage_Sales_Block_Order_Invoice_Totals {

	/**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
		parent::_initTotals();
		
		//fetch the quote id
		$_quoteId = $this->getOrder()->getQuoteId();
		
		/**
		 * Base surcharge amount
		 */
		$quoteAddress = Mage::getSingleton('sales/quote_address')
				->getCollection()
				->addFieldToFilter('quote_id',$_quoteId)
				->addFieldToFilter('address_type','shipping');
				
		foreach($quoteAddress as $quoteInfo) {
			$this->_totals['base_surcharge_amount'] = new Varien_Object(array(
				'code'  => 'base_surcharge_amount',
				'field'  => 'base_surcharge_amount',
				'strong'=> false,
				'value' => $quoteInfo->getBaseSurchargeAmount(),
				'label' => $this->__('Surcharge Additional Fee')
			));
		}
		
		$this->addTotalBefore($this->_totals['base_surcharge_amount'], 'grand_total');
        //$this->removeTotal('base_grandtotal');
		
        return $this;
    }
	
}
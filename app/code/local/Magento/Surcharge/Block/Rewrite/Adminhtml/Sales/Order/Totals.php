<?php
class Magento_Surcharge_Block_Rewrite_Adminhtml_Sales_Order_Totals extends Mage_Adminhtml_Block_Sales_Order_Totals
{
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
        parent::_initTotals();

		$amount = Mage::helper('magento_surcharge')->calculateSurchargeAmount($this->getSource()->getSubtotal());
		
        if ($amount) {
            $this->addTotalBefore(new Varien_Object(array(
                'code'      => 'magento_surcharge',
                'value'     => $amount,
                'base_value'=> $amount,
                'label'     => $this->helper('magento_surcharge')->__('Surcharge Additional Fee'),
            ), array('shipping', 'tax'))); //add surcharge amount before tax, shipping
        }
 
        return $this;
    }
 
}
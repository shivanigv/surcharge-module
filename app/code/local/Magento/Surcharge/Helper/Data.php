<?php
class Magento_Surcharge_Helper_Data extends Mage_Core_Helper_Abstract {

	/**
	 * calculate surcharge amount
	 *
	 * @return float
	 */
	public function calculateSurchargeAmount($_subTotal) {
		$_surchargeAmount = (5/100); //needs to be dynamic
		
		$amount = ($_surchargeAmount)*($_subTotal);
		
		return $amount;
	}

}